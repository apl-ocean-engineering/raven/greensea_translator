cmake_minimum_required(VERSION 3.0.2)
project(greensea_translator)

find_package(catkin REQUIRED COMPONENTS
  marine_acoustic_msgs
  apl_msgs
  dspl_sealite_driver_ros  # For sealite msgs/srvs
  geometry_msgs
  greensea_msgs
  md_actuator_msgs  # For tilt status msgs
  nav_msgs
  rosgraph_msgs
  sensor_msgs
  rospy
  tf_conversions
  tf2_ros
)

## Uncomment this if the package has a setup.py. This macro ensures
## modules and global scripts declared therein get installed
## See http://ros.org/doc/api/catkin/html/user_guide/setup_dot_py.html
catkin_python_setup()

## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if your package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES greensea_translator
#  CATKIN_DEPENDS marine_acoustic_msgs rospy
#  DEPENDS system_lib
)

## Mark executable scripts (Python etc.) for installation
## in contrast to setup.py, you can choose the destination
catkin_install_python(PROGRAMS
  nodes/gss_translator
  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
 )

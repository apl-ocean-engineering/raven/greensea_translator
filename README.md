# greensea_translator

This package provides a ROS API to UW APL's heavily-modified
SAAB Falcon.

It assumes that you have installed Greensea's LCM messages on
your PYTHONPATH. We have been asked not to publicly share their ICDs,
so the message definitions themselves are not part of this repo.

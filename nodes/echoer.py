#! /usr/bin/env python3

import lcm

from gss import pcomms_t
from gss import water_object_list_t


class Echoer(object):
    def __init__(self):
        self.lc = lcm.LCM()

        self.wypts_stat_sub = self.lc.subscribe("WYPTS_STAT", self.handle_wypts_stat)

        self.lc.subscribe("NETWORKING_TEST", self.echo)
        self.lc.subscribe("WYPTS_CMD", self.echo)
        self.lc.subscribe("OPENMNGR_CMD", self.echo)

        # self.lc.subscribe("FF_EFFORT", self.echo)

        # self.lc.subscribe("SAAB_FALCON_PCS_CMD", self.handle_pcomms)

    def run(self):
        try:
            while True:
                self.lc.handle()
        except KeyboardInterrupt:
            pass

    def echo(self, channel, data):
        print("Got {} message!".format(channel))
        pass

    def handle_wypts_stat(self, channel, data):
        msg = water_object_list_t.decode(data)
        if len(msg.objects) > 0:
            print("Got {} message!".format(channel))
        for idx, obj in enumerate(msg.objects):
            print(
                "{}-th waypoint ({}) is at {}, {} ({})".format(
                    idx,
                    obj.name,
                    obj.position_x,
                    obj.position_y,
                    obj.position_coordinate_system,
                )
            )

    def handle_pcomms(self, channel, data):
        lcm_msg = pcomms_t.decode(data)
        for aa in lcm_msg.analogs:
            if aa.name == "U_CAM_TILT_CMD":
                print("U_CAM_TILT_CMD: {}".format(aa.value))


if __name__ == "__main__":
    ff = Echoer()
    ff.run()

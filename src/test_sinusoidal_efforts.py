#! /usr/bin/env python3

"""
Send a sinusoidal effort command to the AUTO_DAQ_CMD channel,
with signal specified by period and min/max values.

This way of injecting commands into the control chain
assumes that there is an arbiter choosing whether to forward
AUTO_DAQ_CMD or OPENMNGR_DAQ_CMD on to openmngr.

So, it does not work with an arbiter on actual thruster commands.
(A replacement in raven_lcm_nodes is NYI.)
"""

import lcm
import numpy as np
import sys
import time

from gss import analog_t
from gss import pcomms_t


class SinusoidGenerator(object):
    def __init__(self, axis, min_val, max_val, period):
        self.axis = axis
        self.avg_val = 0.5 * (min_val + max_val)
        self.amplitude = -0.5 * (max_val - min_val)
        self.period = period
        self.daq_topic = "AUTO_DAQ_CMD"
        self.enable_topic = "ENABLE_AUTO_DAQ_CMD"
        self.sleep_dt = 0.1  # Run at 10 Hz
        self.t0 = time.time()
        self.lc = lcm.LCM()

    def run(self):
        enable_msg = pcomms_t()  # We just use an empty message for this
        self.lc.publish(self.enable_topic, enable_msg.encode())
        while True:
            tt = time.time() - self.t0
            val = self.avg_val + self.amplitude * np.sin(2 * np.pi * tt / self.period)
            lcm_cmd = pcomms_t()
            for axis in ["X", "Y", "Z", "PSI", "THETA", "PHI"]:
                aa = analog_t()
                aa.name = "U_DAQ_JOY_" + axis
                if axis == self.axis:
                    aa.value = val
                else:
                    aa.value = 0.0
                lcm_cmd.analogs.append(aa)
            lcm_cmd.num_analogs = len(lcm_cmd.analogs)
            self.lc.publish(self.daq_topic, lcm_cmd.encode())
            # QUESTION: Does LCM provide a better way to publish at a fixed rate?
            time.sleep(self.sleep_dt)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "axis", type=str, help="Which axis to control. {X, Y, Z, PSI, THETA, PHI}"
    )
    parser.add_argument("min_val", type=float, help="minimum value")
    parser.add_argument("max_val", type=float, help="maximum value")
    parser.add_argument("period", type=float, help="period of oscillation")
    args = parser.parse_args()

    if args.axis not in ["X", "Y", "Z", "PSI", "THETA", "PHI"]:
        print("ERROR: invalid input axis ({})".format(args.axis))
        sys.exit()
    sg = SinusoidGenerator(args.axis, args.min_val, args.max_val, args.period)
    sg.run()

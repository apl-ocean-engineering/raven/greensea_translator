#! /usr/bin/env python3

"""
Send an impulse effort command to the AUTO_DAQ_CMD channel,
with signal specified by delay, amplitude, and width.

This way of injecting commands into the control chain
assumes that there is an arbiter choosing whether to forward
AUTO_DAQ_CMD or OPENMNGR_DAQ_CMD on to openmngr.

So, with an arbiter on actual thruster commands, it has been
replaced by pub_impulse_effort.py (in raven_lcm_nodes).
"""

import lcm
import sys
import time

from gss import analog_t
from gss import pcomms_t


class ImpulseGenerator(object):
    def __init__(self, axis, max_val, delay, width):
        self.axis = axis
        self.max_val = max_val
        self.delay = delay
        self.width = width
        self.daq_topic = "AUTO_DAQ_CMD"
        self.enable_topic = "ENABLE_AUTO_DAQ_CMD"
        self.sleep_dt = 0.1  # Run at 10 Hz
        self.t0 = time.time()
        self.lc = lcm.LCM()

    # TODO: Make this actually repeat the impulses, rather than just one-shot.
    def run(self):
        # NB: It looks like we do NOT need to disable any autos for this to work; effort gets piped through cleanly.
        enable_msg = pcomms_t()  # We just use an empty message for this
        self.lc.publish(self.enable_topic, enable_msg.encode())
        while True:
            tt = time.time() - self.t0
            cmd = 0.0
            if tt > self.delay and tt < self.delay + self.width:
                cmd = self.max_val

            lcm_cmd = pcomms_t()
            for axis in ["X", "Y", "Z", "PSI", "THETA", "PHI"]:
                aa = analog_t()
                aa.name = "U_DAQ_JOY_" + axis
                if axis == self.axis:
                    aa.value = cmd
                else:
                    aa.value = 0.0
                lcm_cmd.analogs.append(aa)
            lcm_cmd.num_analogs = len(lcm_cmd.analogs)
            self.lc.publish(self.daq_topic, lcm_cmd.encode())
            # QUESTION: Does LCM provide a better way to publish at a fixed rate?
            time.sleep(self.sleep_dt)

            if tt > self.delay + self.width:
                break


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "axis", type=str, help="Which axis to control. {X, Y, Z, PHI, THETA, PSI}"
    )
    parser.add_argument("max_val", type=float, help="maximum value")
    parser.add_argument("delay", type=float, help="delay before impulse")
    parser.add_argument("width", type=float, help="width of impulse")
    args = parser.parse_args()

    if args.axis not in ["X", "Y", "Z", "PSI", "THETA", "PHI"]:
        print("ERROR: invalid input axis ({})".format(args.axis))
        sys.exit()
    sg = ImpulseGenerator(args.axis, args.max_val, args.delay, args.width)
    sg.run()

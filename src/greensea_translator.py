#! /usr/bin/env python3

import lcm
import numpy as np
import pyproj
import rospy
import tf_conversions
from tf_conversions import transformations
import tf2_ros

from gss import analog_t
from gss import digital_t
from gss import daq_data_t  # XBOX
from gss import dvl_stat_t  # DVL data from sensor
from gss import effort_t  # OPENCMD_EFFORT
from gss import imu_stat_t  # IMU data from sensor
from gss import nav_solution_t  # Output of OPENINS; overall nav estimate
from gss import pcomms_t  # Used for SAAB_FALCON_CMD, SAAB_FALCON_STAT
from gss import pose_stat_t  # USBL and GPS data; individual waypoint goals
from gss import pressure_stat_t

from marine_acoustic_msgs.msg import Dvl
from apl_msgs.msg import JointPositionCommand
from dspl_sealite_driver_ros.msg import SealiteStatus
from dspl_sealite_driver_ros.srv import SealiteIncrement, SealiteLevel
from geometry_msgs.msg import PointStamped  # For publishing USBL data
from geometry_msgs.msg import PoseStamped  # For publishing single goal pose
from geometry_msgs.msg import TransformStamped
from geometry_msgs.msg import WrenchStamped  # OPENCMD_EFFORT
from greensea_msgs.msg import NavSolution  # OPENINS_NAV_SOLUTION
from greensea_msgs.msg import ThrusterCmd  # {SAAB,GSS,ROS}_THRUSTER_CMD
from md_actuator_msgs.msg import MdActuatorIncrementCommand, StatusResponse
from nav_msgs.msg import Odometry
from rosgraph_msgs.msg import Clock
from sensor_msgs import point_cloud2  # Helper functions for PointCloud2 msgs
from sensor_msgs.msg import FluidPressure  # NORTEK_PRESSURE_STAT
from sensor_msgs.msg import Imu  # OPENINS_IMU_STAT
from sensor_msgs.msg import Joy  # TOPSIDE_XBOX_STAT
from sensor_msgs.msg import JointState  # SAAB_FALCON_STAT (tilt position)
from sensor_msgs.msg import NavSatFix  # OPENINS_GPS_STAT
from sensor_msgs.msg import PointCloud2  # Used for DVL


def make_pcomms(analogs, digitals):
    msg = pcomms_t()
    msg.num_analogs = len(analogs)
    for key, value in analogs.items():
        analog = analog_t()
        analog.name = key
        analog.value = value
        msg.analogs.append(analog)
    msg.num_digitals = len(digitals)
    for key, value in digitals.items():
        digital = digital_t()
        digital.name = key
        digital.value = value
        msg.digitals.append(digital)
    return msg


class GreenseaTranslator(object):
    def __init__(self, replay=False, sim=False):
        self.replay = replay
        self.sim = sim
        self.initialized = False
        # What step (in % of total brightness) should be requested on increment/decrement
        # TODO(lindzey): make this a parameter.
        self.increment_step = 5

        self.lc = lcm.LCM()

        # LCM Subscriptions
        self.imu_sub = self.lc.subscribe("OPENINS_IMU_STAT", self.handle_imu)
        self.dvl_sub = self.lc.subscribe("OPENINS_DVL_STAT", self.handle_dvl)
        self.nav_sub = self.lc.subscribe("OPENINS_NAV_SOLUTION", self.handle_nav)
        self.pcs_sub = self.lc.subscribe("SAAB_FALCON_PCS_CMD", self.handle_pcs)
        self.pressure_sub = self.lc.subscribe(
            "NORTEK_PRESSURE_STAT", self.handle_pressure
        )
        self.usbl_sub = self.lc.subscribe("OPENINS_USBL_STAT", self.handle_usbl)
        self.effort_sub = self.lc.subscribe("OPENCMD_EFFORT", self.handle_effort)
        self.robot_gps_sub = self.lc.subscribe(
            "OPENINS_GPS_STAT", self.handle_robot_gps
        )

        # I haven't settled on which direction the joystick translation needs to go...
        # TODO(lindzey): Make this a parameter
        use_gss_joy = True
        if use_gss_joy:
            self.gss_joy_sub = self.lc.subscribe("ROS_XBOX_STAT", self.handle_gss_joy)
            self.ros_joy_pub = rospy.Publisher("joy", Joy, queue_size=1)
        else:
            self.ros_joy_sub = rospy.Subscriber("joy", Joy, self.handle_ros_joy)

        # TODO: Make this a parameter
        robot_name = "raven"

        # Translate thruster commands from ROS -> LCM
        thrust_topic = "/{}/thruster_command".format(robot_name)
        self.thrust_sub = rospy.Subscriber(
            thrust_topic, ThrusterCmd, self.handle_thrust_cmd
        )

        # ROS Subscriptions
        goal_topic = "/{}/goal_pose".format(robot_name)
        self.goal_sub = rospy.Subscriber(goal_topic, PoseStamped, self.handle_waypoint)

        light_status_topic = "/{}/lights/sealite_status".format(robot_name)
        self.light_sub = rospy.Subscriber(
            light_status_topic, SealiteStatus, self.handle_light_status
        )
        tilt_status_topic = "/{}/tilt/status".format(robot_name)
        self.tilt_status_sub = rospy.Subscriber(
            tilt_status_topic, StatusResponse, self.handle_tilt_status
        )
        # ROS Publications
        dvl_topic = "/{}/dvl".format(robot_name)
        self.dvl_pub = rospy.Publisher(dvl_topic, Dvl, queue_size=1)
        dvl_pc2_topic = "/{}/dvl_points".format(robot_name)
        self.dvl_pc2_pub = rospy.Publisher(dvl_pc2_topic, PointCloud2, queue_size=1)
        nav_topic = "/{}/openins_nav".format(robot_name)
        self.nav_pub = rospy.Publisher(nav_topic, NavSolution, queue_size=1)
        odom_topic = "/{}/odom".format(robot_name)
        self.odom_pub = rospy.Publisher(odom_topic, Odometry, queue_size=1)
        imu_topic = "/{}/imu".format(robot_name)
        self.imu_pub = rospy.Publisher(imu_topic, Imu, queue_size=1)
        pressure_topic = "/{}/pressure".format(robot_name)
        self.pressure_pub = rospy.Publisher(pressure_topic, FluidPressure, queue_size=1)
        usbl_topic = "/{}/usbl_point".format(robot_name)
        self.usbl_pub = rospy.Publisher(usbl_topic, PointStamped, queue_size=1)
        effort_topic = "/{}/effort".format(robot_name)
        self.effort_pub = rospy.Publisher(effort_topic, WrenchStamped, queue_size=1)
        robot_gps_topic = "/{}/gps".format(robot_name)
        self.robot_gps_pub = rospy.Publisher(robot_gps_topic, NavSatFix, queue_size=1)
        joint_state_topic = "/{}/joint_states".format(robot_name)
        self.joint_state_pub = rospy.Publisher(
            joint_state_topic, JointState, queue_size=1
        )
        tilt_cmd_topic = "/{}/tilt/command/position".format(robot_name)
        self.tilt_cmd_pub = rospy.Publisher(
            tilt_cmd_topic, JointPositionCommand, queue_size=1
        )
        tilt_increment_topic = "/{}/tilt/command/native/increment".format(robot_name)
        self.tilt_increment_pub = rospy.Publisher(
            tilt_increment_topic, MdActuatorIncrementCommand, queue_size=1
        )

        # For some reason, the service proxy prepends raven
        light_level_service = "/{}/lights/sealite_level".format(robot_name)
        try:
            # 2 seconds is not enough, since we don't know what order roslaunch will start things
            rospy.wait_for_service(light_level_service, timeout=5.0)
            self.light_level_service_proxy = rospy.ServiceProxy(
                light_level_service, SealiteLevel
            )
        except rospy.ROSException as ex:
            rospy.logerr(ex)
            self.light_level_service_proxy = None

        light_increment_service = "/{}/lights/sealite_increment".format(robot_name)
        try:
            # 2 seconds is not enough, since we don't know what order roslaunch will start things
            rospy.wait_for_service(light_increment_service, timeout=5.0)
            self.light_increment_service_proxy = rospy.ServiceProxy(
                light_increment_service, SealiteIncrement
            )
        except rospy.ROSException as ex:
            rospy.logerr(ex)
            self.light_increment_service_proxy = None

        self.clock_pub = rospy.Publisher("/clock", Clock, queue_size=1)

        self.gss_frame = "gss_origin"
        # I'm assuming that this coincides with the IMU's origin
        self.vehicle_frame = "raven/base_link"
        self.br = tf2_ros.TransformBroadcaster()

    def run(self):
        try:
            while not rospy.is_shutdown():
                self.lc.handle()
        except KeyboardInterrupt:
            pass
        # TODO: Is it important to unsubscribe from all of them?
        # self.lc.unsubscribe(self.example_sub)

    def initialize_projection(self, lon, lat):
        """
        Initialize the transverse mercator projection that is used within ROS.
        """
        local_proj_string = "+proj=tmerc +lat_0={} +lon_0={}".format(lat, lon)
        self.local_proj = pyproj.Proj(local_proj_string)
        self.gss_proj = pyproj.Proj(proj="utm", zone=10, ellps="WGS84")
        self.initialized = True

    def local_xy_from_lonlat(self, lon, lat):
        easting, northing = self.local_proj(lon, lat)
        # NED coordinate frame
        return northing, easting

    def gss_xy_from_lonlat(self, lon, lat):
        easting, northing = self.gss_proj(lon, lat)
        # NED coordinate frame
        return northing, easting

    def lonlat_from_local_xy(self, xx, yy):
        lon, lat = self.local_proj(yy, xx, inverse=True)
        return lon, lat

    def handle_ros_joy(self, msg):
        daq_msg = daq_data_t()
        daq_msg.unix_time = msg.header.stamp.to_sec()
        daq_msg.have_comms = (
            True  # Always true; their driver simply doesn't publish if no joystick
        )
        daq_msg.analog_resolution = 65535
        daq_msg.analogs = 8 * [0]
        daq_msg.nanalog = len(daq_msg.analogs)
        daq_msg.digitals = 11 * [0]
        daq_msg.ndigital = len(daq_msg.digitals)

        # LCM uses True/False, ROS uses 1/0
        for idx, val in enumerate(msg.buttons):
            daq_msg.digitals[idx] = bool(val)

        # Greensea has some weird behavior here.
        # Duplicating exactly, rather than trying to make it sane.
        for idx in [0, 1, 3, 4]:
            # ROS 1 -> -1 maps to LCM -1 -> 65534
            daq_msg.analogs[idx] = int(0.5 * 65535 * (1 - msg.axes[idx])) - 1
        for idx in [2, 5, 6]:
            # ROS 1 -> -1 maps to LCM 0-> 65535
            daq_msg.analogs[idx] = int(0.5 * 65535 * (1 - msg.axes[idx]))
        for idx in [7]:
            # ROS 1 -> -1 maps to LCM 65535 -> 0
            daq_msg.analogs[idx] = int(0.5 * 65535 * (1 + msg.axes[idx]))

        self.lc.publish("TOPSIDE_XBOX_STAT", daq_msg.encode())

    def handle_gss_joy(self, _channel, data):
        daq_msg = daq_data_t.decode(data)
        joy_msg = Joy()
        # Not used...in the joy node, it gives the device (e.g. /dev/input/js0)
        joy_msg.header.frame_id = "joy"
        joy_msg.header.stamp = rospy.Time.from_sec(daq_msg.unix_time)

        # LCM uses True/False, ROS uses 1/0
        joy_msg.buttons = [int(val) for val in daq_msg.digitals]

        # Greensea has some weird behavior here.
        # Duplicating exactly, rather than trying to make it sane.
        joy_msg.axes = 11 * [0]
        for idx in [0, 1, 3, 4]:
            # ROS 1 -> -1 maps to LCM -1 -> 65534
            joy_msg.axes[idx] = 1 - (2.0 / 65535) * (1 + daq_msg.analogs[idx])
        for idx in [2, 5, 6]:
            # ROS 1 -> -1 maps to LCM 0-> 65535
            joy_msg.axes[idx] = 1 - (2.0 / 65535) * daq_msg.analogs[idx]
        for idx in [7]:
            # ROS 1 -> -1 maps to LCM 65535 -> 0
            joy_msg.axes[idx] = (2.0 / 65535) * daq_msg.analogs[idx] - 1

        self.ros_joy_pub.publish(joy_msg)

    def handle_imu(self, channel, data):
        lcm_msg = imu_stat_t.decode(data)
        ros_msg = Imu()
        ros_msg.header.stamp = rospy.Time.from_sec(lcm_msg.time_unix_sec)
        ros_msg.header.frame_id = self.vehicle_frame

        if lcm_msg.attitude_valid:
            roll_rad = np.radians(lcm_msg.attitude_roll_deg)
            pitch_rad = np.radians(lcm_msg.attitude_pitch_deg)
            yaw_rad = np.radians(lcm_msg.attitude_heading_deg)
            quat = transformations.quaternion_from_euler(roll_rad, pitch_rad, yaw_rad)
            ros_msg.orientation.x = quat[0]
            ros_msg.orientation.y = quat[1]
            ros_msg.orientation.z = quat[2]
            ros_msg.orientation.w = quat[3]
            # LCM message doesn't include covariances, so using values from
            # the Sparton's datasheet:
            # "Dynamic Heading/Roll/Pitch Accuracy 1.0 deg RMS"
            # Message is row-major about x, y, z axes:
            ros_msg.orientation_covariance[0] = np.radians(1.0) ** 2
            ros_msg.orientation_covariance[4] = np.radians(1.0) ** 2
            ros_msg.orientation_covariance[8] = np.radians(1.0) ** 2
        else:
            rospy.logwarn("Received IMU message with invalid orientation")
            ros_msg.orientation_covariance[0] = -1

        if lcm_msg.rotational_vel_valid:
            ros_msg.angular_velocity.x = np.radians(lcm_msg.rotational_vel_roll_deg_sec)
            ros_msg.angular_velocity.y = np.radians(
                lcm_msg.rotational_vel_pitch_deg_sec
            )
            ros_msg.angular_velocity.z = np.radians(
                lcm_msg.rotational_vel_heading_deg_sec
            )
            # TODO: How to get covariance?
        else:
            rospy.logwarn("Received IMU message with invalid rotational velocity")
            ros_msg.angular_velocity_covariance[0] = -1

        if lcm_msg.acceleration_valid:
            ros_msg.linear_acceleration.x = lcm_msg.acceleration_x_m_sec2
            ros_msg.linear_acceleration.y = lcm_msg.acceleration_y_m_sec2
            ros_msg.linear_acceleration.z = lcm_msg.acceleration_z_m_sec2
            # TODO: The data sheet reports Accelerometer Noise Density,
            #       Bias Stability, and Velocity Random Walk. I don't know
            #       how to get variances from that.
            # The linear_acceleration_covariance matrix defaults to all-zero,
            # which will be interpreted as "unknown"
        else:
            rospy.logwarn("Received IMU message with invalid linear acceleration")
            ros_msg.linear_acceleration_covariance[0] = -1

        self.imu_pub.publish(ros_msg)

    def handle_pressure(self, _, data):
        """
        TODO: The options are OPENINS_DEPTH_STAT which is a depth_stat_t
              or NORTEK_PRESSURE_STAT which is a pressure_stat_t.
              fix this the next time the vehicle is in the water.
        Translate OPENINS_PRESSURE_STAT to ROS message.
        """
        lcm_msg = pressure_stat_t.decode(data)
        print("Got Pressure/Depth data")
        ros_msg = FluidPressure()
        ros_msg.header.stamp = rospy.Time.from_sec(lcm_msg.time_unix_sec)
        ros_msg.header.frame_id = "raven/dvl"
        pascals_per_psi = 6894.76
        ros_msg.fluid_pressure = lcm_msg.pressure_psi * pascals_per_psi
        self.pressure_pub.publish(ros_msg)

    def handle_robot_gps(self, _, data):
        lcm_msg = pose_stat_t.decode(data)
        rospy.loginfo_once("Got GPS data")
        ros_msg = NavSatFix()
        ros_msg.header.stamp = rospy.Time.from_sec(lcm_msg.time_unix_sec)
        ros_msg.header.frame_id = "raven/gps"
        # rospy.logwarn_throttle(5, "GPS NYI! message = {}".format(lcm_msg))
        # TODO: finish filling this in!
        self.robot_gps_pub.publish(ros_msg)

    def handle_usbl(self, _, data):
        """
        Translate OPENINS_USBL_STAT to a ROS PointStamped message.

        This isn't USBL data as I'd normally think of it -- rather than
        a raw vector from the topside unit to the subsea transceiver,
        it has already been corrected using a topside GPS and IMU to be
        an estimate of the robot's position in geographic coordinates.

        Since it doesn't include enough information to be another frame,
        I am simply publishing the USBL's estimate of the robot position
        represented in the GSS frame.
        (However, OPENINS w/ USBL could be an additional frame)
        """
        lcm_msg = pose_stat_t.decode(data)
        print("Got USBL data")
        rospy.logwarn("TODO: define and use ROS message for USBL!")
        if not self.initialized:
            rospy.logwarn("Got USBL data before projection is initialized.")
            return
        ros_msg = PointStamped()
        ros_msg.header.stamp = rospy.Time.from_sec(lcm_msg.time_unix_sec)
        ros_msg.header.frame_id = self.gss_frame

        # QUESTION: The comments in pose_stat_t.lcm offer 'geographic',
        #           'relative', etc. as options, but no mention is made of
        #           'absolute', and I'm not sure what that frame is.
        if lcm_msg.position_coordinate_system != "absolute":
            rospy.logwarn("Got USBL data in non-geographic coordinate system!")
            return
        lon, lat = lcm_msg.position_x, lcm_msg.position_y
        northing, easting = self.local_xy_from_lonlat(lon, lat)
        ros_msg.point.x = northing
        ros_msg.point.y = easting
        ros_msg.point.z = lcm_msg.position_z
        self.usbl_pub.publish(ros_msg)

    def handle_effort(self, _, data):
        """
        Translate OPENCMD_EFFORT to ROS WrenchStamped message.

        Currently, this isn't truly a wrench because we don't know the mapping
        from 100% effort to forces/torques. However, this seemed the most
        appropriate message, and one that's supported by rviz.

        TODO: Consider updating this if/when we get
        (At a minimum, we could properly scale 100% along x/y/z axes based
        on thruster geometry)
        """
        lcm_msg = effort_t.decode(data)
        ros_msg = WrenchStamped()
        ros_msg.header.stamp = rospy.Time.from_sec(lcm_msg.time_unix_sec)
        ros_msg.header.frame_id = self.vehicle_frame

        linear_scale = 0.02
        angular_scale = 0.05
        ros_msg.wrench.force.x = linear_scale * lcm_msg.effort_x
        ros_msg.wrench.force.y = linear_scale * lcm_msg.effort_y
        ros_msg.wrench.force.z = linear_scale * lcm_msg.effort_z
        # NOTE: x/y aren't actually controlled.
        ros_msg.wrench.torque.x = angular_scale * lcm_msg.effort_phi
        ros_msg.wrench.torque.y = angular_scale * lcm_msg.effort_theta
        ros_msg.wrench.torque.z = angular_scale * lcm_msg.effort_psi

        self.effort_pub.publish(ros_msg)

    def handle_nav(self, _, data):
        """
        Translate OPENINS_NAV_SOLN to ROS messages.

        The nav data includes more fields than any single standard ROS message.
        For now, publishing messages that already have corresponding rviz
        plugins, though we may want to add more.
        * tf
        * odometry
        """
        lcm_msg = nav_solution_t.decode(data)
        if not self.initialized:
            lon, lat = lcm_msg.initial_lonlat_fix
            self.initialize_projection(lon, lat)

        self.publish_nav(lcm_msg)
        self.publish_odom(lcm_msg)
        self.publish_tf(lcm_msg)
        if self.replay:
            self.publish_clock(lcm_msg)

    def handle_pcs(self, _, data):
        """
        Translate SAAB_FALCON_PCS_CMD to light and tilt commands.

        The topics and field names hard-coded here need to match
        the workspace and joystick configs:
        * workspace/config_components/vehicle_component.yml
        * gss_signal_mapper/uw_falcon_maps.yml
        """
        lcm_msg = pcomms_t.decode(data)
        analogs_dict = {aa.name: aa.value for aa in lcm_msg.analogs}
        digitals_dict = {dd.name: dd.value for dd in lcm_msg.digitals}

        # First, parse any tilt commands
        # Command for absolute position coming from Workspace slider
        if "U_CAM_TILT_CMD" in analogs_dict:
            tilt_cmd = np.radians(analogs_dict["U_CAM_TILT_CMD"])
            tilt_msg = JointPositionCommand()
            tilt_msg.name.append("tilt_joint")
            tilt_msg.position.append(tilt_cmd)
            self.tilt_cmd_pub.publish(tilt_msg)
        # Command for relative motion coming from joystick
        increment = np.radians(5.0)
        duty_cycle = 0.1
        if "U_CAM_TILT_DOWN" in analogs_dict and analogs_dict["U_CAM_TILT_DOWN"] != 0:
            msg = MdActuatorIncrementCommand()
            msg.increment = -1 * increment
            msg.duty_cycle = duty_cycle
            self.tilt_increment_pub.publish(msg)
        if "U_CAM_TILT_UP" in analogs_dict and analogs_dict["U_CAM_TILT_UP"] != 0:
            msg = MdActuatorIncrementCommand()
            msg.increment = increment
            msg.duty_cycle = duty_cycle
            self.tilt_increment_pub.publish(msg)

        # Next, parse any light commands
        # Handle absolute commands
        light_cmds = {}
        if "U_PORT_LIGHT_BRIGHTNESS" in analogs_dict:
            light_cmds["left"] = int(round(analogs_dict["U_PORT_LIGHT_BRIGHTNESS"]))
        if "U_STBD_LIGHT_BRIGHTNESS" in analogs_dict:
            light_cmds["right"] = int(round(analogs_dict["U_STBD_LIGHT_BRIGHTNESS"]))
        if self.light_level_service_proxy is not None and len(light_cmds) > 0:
            lights = list(light_cmds.keys())
            cmds = [light_cmds[light] for light in lights]
            try:
                self.light_level_service_proxy(lights, cmds)
            except rospy.ServiceException as ex:
                rospy.logerr("Could not process request: " + str(ex))
        # Handle relative (increment) commands
        light_increment = None
        if "U_INCREASE_LIGHTS" in digitals_dict:
            if digitals_dict["U_INCREASE_LIGHTS"]:
                if light_cmds:
                    err_msg = (
                        "Received absolute brightness and a command to "
                        "increase brightness in same message."
                    )
                    rospy.logerr(err_msg)
                light_increment = self.increment_step
        if "U_DECREASE_LIGHTS" in digitals_dict:
            if digitals_dict["U_DECREASE_LIGHTS"]:
                if light_cmds:
                    err_msg = (
                        "Received absolute brightness and a command to "
                        "decrease brightness in same message."
                    )
                    rospy.logerr(err_msg)
                light_increment = -1 * self.increment_step
        if (
            self.light_increment_service_proxy is not None
            and light_increment is not None
        ):
            try:
                self.light_increment_service_proxy(["all"], light_increment)
            except rospy.ServiceException as ex:
                rospy.logerr("Could not process request: " + str(ex))

    def publish_clock(self, lcm_msg):
        """
        Required when replaying data.
        """
        clock_msg = Clock()
        clock_msg.clock = rospy.Time.from_sec(lcm_msg.unix_time)
        self.clock_pub.publish(clock_msg)

    def publish_nav(self, lcm_msg):
        """
        Publish message that captures the GSS messages field-for-field
        """
        ros_msg = NavSolution()
        ros_msg.unix_time = rospy.Time.from_sec(lcm_msg.unix_time)

        # TODO: May want to extract this into more generic conversion
        #       function if any other messages need it.
        #       NB: This may be a little more annoying bcause the LCM
        #           field for timestamp is NOT consistently named
        for attr in dir(lcm_msg):
            # These are functions defined on any LCM message
            if attr in ["encode", "decode"]:
                continue
            if attr.startswith("_"):
                continue
            # Already dealt with the time's special case
            if attr == "unix_time":
                continue
            if attr not in dir(ros_msg):
                errmsg = "Attribute {} appears in {} but not {}".format(
                    attr, type(lcm_msg), type(ros_msg)
                )
                rospy.logerr_throttle(5.0, errmsg)
                raise Exception(errmsg)
            # LCM uses tuples, while ROS uses lists
            if type(getattr(lcm_msg, attr)) == tuple:
                setattr(ros_msg, attr, list(getattr(lcm_msg, attr)))
            else:
                ros_attr_type = type(getattr(ros_msg, attr))
                lcm_attr_type = type(getattr(lcm_msg, attr))
                if not isinstance(getattr(lcm_msg, attr), ros_attr_type):
                    raise Exception(
                        "Attribute {} has mismatched type! ROS = {}, lcm = {}".format(
                            attr, ros_attr_type, lcm_attr_type
                        )
                    )
                setattr(ros_msg, attr, getattr(lcm_msg, attr))
        self.nav_pub.publish(ros_msg)

    def publish_odom(self, lcm_msg):
        odom_msg = Odometry()
        odom_msg.header.stamp = rospy.Time.from_sec(lcm_msg.unix_time)
        odom_msg.header.frame_id = self.gss_frame
        odom_msg.child_frame_id = self.vehicle_frame

        # TODO: Check with Heath about relative/absolute variables. If it works
        #    as I expect, we should just use absolute_position
        # TODO: Flip 0/1 back after they've fixed the END/NED bug in their simulator
        # if self.sim:
        #    odom_msg.pose.pose.position.x = lcm_msg.relative_position[1]
        #    odom_msg.pose.pose.position.y = lcm_msg.relative_position[0]
        #    odom_msg.pose.pose.position.z = lcm_msg.relative_position[2]
        # else:
        #    odom_msg.pose.pose.position.x = lcm_msg.relative_position[0]
        #    odom_msg.pose.pose.position.y = lcm_msg.relative_position[1]
        #    odom_msg.pose.pose.position.z = lcm_msg.relative_position[2]

        # Forget relative, try using absolute!
        lon, lat, depth = lcm_msg.absolute_position
        xx, yy = self.local_xy_from_lonlat(lon, lat)
        odom_msg.pose.pose.position.x = xx
        odom_msg.pose.pose.position.y = yy
        odom_msg.pose.pose.position.z = depth
        # print("Publishing odom: xx,yy = {:0.2f}, {:0.2f}".format(xx, yy))

        roll, pitch, yaw = map(np.radians, lcm_msg.attitude)
        quat = tf_conversions.transformations.quaternion_from_euler(roll, pitch, yaw)
        # print("For attitude = {:0.2f},{:0.2f}, {:0.2f}".format(*lcm_msg.attitude))
        # print("  quat = {:0.2f}, {:0.2f}, {:0.2f}, {:0.2f}".format(*quat))
        odom_msg.pose.pose.orientation.x = quat[0]
        odom_msg.pose.pose.orientation.y = quat[1]
        odom_msg.pose.pose.orientation.z = quat[2]
        odom_msg.pose.pose.orientation.w = quat[3]

        # TODO: Is there a conversion from imu_error / dvl_error to covariance?
        odom_msg.pose.covariance[0] = -1

        # TODO: Figure out how to go from relative_position_dot + attitude to
        #  vehicle-relative velocities. What axis is velocity w/r/t?
        # Confusingly, when the vehicle is moving WNW, relative_position[0] is
        # incrementing faster than [1], which suggests an ENU frame, but
        # the relative_position_dot has [1] faster tahn [1], with the right
        # signs for a NED frame.
        # (I've emailed Heath about this, and he thinks that it's a bug in
        #        the simulator and that GSS uses NED everywhere)

        # Convert velocity from absolute NED frame to vehicle-relative frame
        rot = tf_conversions.transformations.quaternion_matrix(quat)
        vel = np.array(lcm_msg.relative_position_dot)
        vel_body = rot[0:3, 0:3] @ vel  # Quaternion matrix is 4x4
        odom_msg.twist.twist.linear.x = vel_body[0]
        odom_msg.twist.twist.linear.y = vel_body[1]
        odom_msg.twist.twist.linear.z = vel_body[2]

        odom_msg.twist.twist.angular.x = lcm_msg.attitude_dot[0]
        odom_msg.twist.twist.angular.y = lcm_msg.attitude_dot[1]
        odom_msg.twist.twist.angular.z = lcm_msg.attitude_dot[2]

        # There aren't any fields in the LCM message that can be used for
        # a covariance on velocities.
        odom_msg.twist.covariance[0] = -1

        self.odom_pub.publish(odom_msg)

    def publish_tf(self, lcm_msg):
        """
        Publish transform from robot to world, in local coordinates

        TODO: In order to support publishing multiple estimates of the
        vehicle's position, we make the vehicle's frame be the root of
        the TF tree.
        """
        tf = TransformStamped()
        tf.header.stamp = rospy.Time.from_sec(lcm_msg.unix_time)
        tf.header.frame_id = self.vehicle_frame
        tf.child_frame_id = self.gss_frame

        # TODO: Logic is duplicated between here and publish_odom. (modulo
        #       negating the transformation to go from base_link -> gss)

        # Take inverse rotation, since the nav message gives world->robot
        # but we're publishing -> robot
        roll, pitch, yaw = map(np.radians, lcm_msg.attitude)
        quat = tf_conversions.transformations.quaternion_from_euler(roll, pitch, yaw)
        inv_quat = tf_conversions.transformations.quaternion_inverse(quat)

        tf.transform.rotation.x = inv_quat[0]
        tf.transform.rotation.y = inv_quat[1]
        tf.transform.rotation.z = inv_quat[2]
        tf.transform.rotation.w = inv_quat[3]

        # Rotate the translation to be in the robot's frame
        lon, lat, depth = lcm_msg.absolute_position
        xx, yy = self.local_xy_from_lonlat(lon, lat)
        rot = tf_conversions.transformations.quaternion_matrix(inv_quat)
        trans = rot[0:3, 0:3] @ np.array([-xx, -yy, -depth])
        tf.transform.translation.x = trans[0]
        tf.transform.translation.y = trans[1]
        tf.transform.translation.z = trans[2]

        self.br.sendTransform(tf)

    def handle_dvl(self, _, data):
        lcm_msg = dvl_stat_t.decode(data)
        ros_msg = Dvl()
        ros_msg.header.stamp = rospy.Time.from_sec(lcm_msg.time_unix_sec)
        ros_msg.header.frame_id = "raven/dvl"
        # TODO: convert lcm_msg.count_publish into ros_msg.header.seq
        # QUESTION: is that OK? Or does it get overwritten by the publisher?
        # => It looks like it's discouraged to set it yourself:
        # https://answers.ros.org/question/55126/why-does-ros-overwrite-my-sequence-number/#:~:text=seq%20field%20in%20e.g.%20the,ros%3A%3APublisher%20class%20itself.
        # https://github.com/ros2/common_interfaces/issues/1#issuecomment-112621348

        # We know that the raven has a Nortek 1000 DVL
        ros_msg.dvl_type = ros_msg.DVL_TYPE_PISTON

        # The Nortek1000 has a 4-beam Janus array convex transducer,
        # with a 25 deg beam angle
        beam_angle = np.radians(25)
        zz = np.cos(beam_angle)
        ss = np.sin(beam_angle) * np.sin(np.pi / 4)
        # NB: Based on photos on the website, the Nortek's beams are
        #     labeled 1-4 CCW, with the connector coming out between 3 & 4.
        # The following vectors assume a coordinate system where
        # x is forward (between beams 1&2) and z is down.
        # This also matches the beam ordering used by WHOI's Gazebo plugin.
        # FWD-STBD
        ros_msg.beam_unit_vec[0].x = ss
        ros_msg.beam_unit_vec[0].y = ss
        ros_msg.beam_unit_vec[0].z = zz
        # FWD_PORT
        ros_msg.beam_unit_vec[1].x = ss
        ros_msg.beam_unit_vec[1].y = -1 * ss
        ros_msg.beam_unit_vec[1].z = zz
        # AFT_PORT
        ros_msg.beam_unit_vec[2].x = -1 * ss
        ros_msg.beam_unit_vec[2].y = -1 * ss
        ros_msg.beam_unit_vec[2].z = zz
        # AFT_STBD
        ros_msg.beam_unit_vec[3].x = -1 * ss
        ros_msg.beam_unit_vec[3].y = ss
        ros_msg.beam_unit_vec[3].z = zz

        # Fields that aren't included in the GSS LCM message
        # TODO: OK to set whole array to -1, or does each field need to be set?
        ros_msg.velocity_covar = [-1 for _ in ros_msg.velocity_covar]

        # TODO: where is sound_speed coming from? Does the message have an "unknown" flag?
        sos = None
        for aa in lcm_msg.analogs:
            if aa.name == "SPEED_OF_SOUND":
                sos = aa.value
        if sos is not None:
            ros_msg.sound_speed = sos

        if len(lcm_msg.analogs) > 0:
            # The logs that were provided by GSS didn't have any analog values;
            # it's possible that the live data will have something interesting
            # there. (I'm particularly interested in temperature/depth/ss
            # from the instrument itself.)
            rospy.logwarn_once(
                "LCM DVL message has analog values: {}".format(
                    [aa.name for aa in lcm_msg.analogs]
                )
            )

        if lcm_msg.lock_btm:
            ros_msg.velocity_mode = ros_msg.DVL_MODE_BOTTOM
            ros_msg.velocity.x = lcm_msg.velocity_btm_x_m_sec
            ros_msg.velocity.y = lcm_msg.velocity_btm_y_m_sec
            ros_msg.velocity.z = lcm_msg.velocity_btm_z_m_sec

            ros_msg.altitude = lcm_msg.altitude_btm_m
            ros_msg.course_gnd = np.arctan2(ros_msg.velocity.y, ros_msg.velocity.x)
            ros_msg.speed_gnd = np.sqrt(
                ros_msg.velocity.x**2 + ros_msg.velocity.y**2
            )

            ros_msg.beam_ranges_valid = True
            ros_msg.beam_velocities_valid = False

            # These are altitudes, so need to correct for slant range
            ros_msg.range[0] = lcm_msg.altitude_btm_beam1_m / zz
            ros_msg.range[1] = lcm_msg.altitude_btm_beam2_m / zz
            ros_msg.range[2] = lcm_msg.altitude_btm_beam3_m / zz
            ros_msg.range[3] = lcm_msg.altitude_btm_beam4_m / zz
            # 999.9 is Greensea's value for no bottom lock
            ros_msg.num_good_beams = sum([1 for rr in ros_msg.range if rr < 999])
            # TODO: For covar, initialize to -1?
            ros_msg.range_covar = [0 for _ in ros_msg.range_covar]

            # TODO: Do we have any beam quality info?
            ros_msg.beam_quality = [-1 for _ in ros_msg.beam_quality]

            # NB: Don't bother filling in raw_velocity or raw_velocity_covar
            #     because their validity flag is set to False

            self.dvl_pub.publish(ros_msg)

            # Create point cloud so detected ranges can accumulate in rviz
            dvl_points = [
                ros_msg.range[ii]
                * np.array(
                    [
                        ros_msg.beam_unit_vec[ii].x,
                        ros_msg.beam_unit_vec[ii].y,
                        ros_msg.beam_unit_vec[ii].z,
                    ]
                )
                for ii in range(4)
                if ros_msg.range[ii] < 999
            ]
            pc2_msg = point_cloud2.create_cloud_xyz32(ros_msg.header, dvl_points)
            self.dvl_pc2_pub.publish(pc2_msg)

        else:
            # rospy.logwarn_throttle(10, "Received DVL message without bottom lock")
            pass

        if lcm_msg.lock_ref:
            ros_msg.velocity_mode = ros_msg.DVL_MODE_WATER
            rospy.logwarn_throttle(10, "NYI: DVL message from GSS has water lock")

    def handle_waypoint(self, pose_msg):
        """
        The ROS control chain generates waypoint goals which are sent to the
        waypoint controller on the vehicle.

        This function translates PoseStamped -> pose_stat_t
        """

        waypoint_msg = pose_stat_t()
        xx_local, yy_local = pose_msg.pose.position.x, pose_msg.pose.position.y
        lon, lat = self.lonlat_from_local_xy(xx_local, yy_local)
        xx_gss, yy_gss = self.gss_xy_from_lonlat(lon, lat)
        waypoint_msg.position_x = xx_gss
        waypoint_msg.position_y = yy_gss
        waypoint_msg.position_z = pose_msg.pose.position.z
        waypoint_msg.position_valid = True

        quat = pose_msg.pose.orientation
        _, _, heading = tf_conversions.transformations.euler_from_quaternion(
            [quat.x, quat.y, quat.z, quat.w]
        )
        waypoint_msg.attitude_roll_deg = 0.0
        waypoint_msg.attitude_pitch_deg = 0.0
        waypoint_msg.attitude_heading_deg = np.degrees(heading)
        waypoint_msg.attitude_valid = True

        self.lc.publish("WAYPOINT_GOAL", waypoint_msg.encode())

    def handle_thrust_cmd(self, ros_msg):
        analogs = dict(zip(ros_msg.names, ros_msg.commands))
        lcm_msg = make_pcomms(analogs, {})
        lcm_msg.time_unix_sec = ros_msg.stamp.to_sec()
        self.lc.publish("ROS_THRUSTER_CMD", lcm_msg.encode())

    def handle_tilt_status(self, ros_msg: StatusResponse) -> None:
        lcm_msg = pcomms_t()
        aa = analog_t()
        aa.name = "TILT_POSITION_DEGREES"
        aa.value = ros_msg.position_degrees
        lcm_msg.analogs.append(aa)
        lcm_msg.num_analogs = len(lcm_msg.analogs)
        self.lc.publish("ROS_TILT_STATUS", lcm_msg.encode())

    def handle_light_status(self, ros_msg):
        light_levels = dict(zip(ros_msg.light, ros_msg.level))
        lcm_msg = pcomms_t()
        # The topic and analog names need to match values in
        # workspace/config_components/vehicle_components.yml's "lights" section
        if "right" in light_levels:
            aa = analog_t()
            aa.name = "U_STBD_LIGHT_BRIGHTNESS"
            aa.value = light_levels["right"]
            lcm_msg.analogs.append(aa)
        if "left" in light_levels:
            aa = analog_t()
            aa.name = "U_PORT_LIGHT_BRIGHTNESS"
            aa.value = light_levels["left"]
            lcm_msg.analogs.append(aa)
        lcm_msg.num_analogs = len(lcm_msg.analogs)

        self.lc.publish("ROS_LIGHT_STATUS", lcm_msg.encode())

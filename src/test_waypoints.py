#! /usr/bin/env python3

import lcm
import pyproj

from gss import analog_t
from gss import digital_t
from gss import nav_solution_t  # Output of OPENINS; overall nav estimate
from gss import pcomms_t  # Used for OPENMNGR_CMD
from gss import water_object_list_t
from gss import water_object_t


def make_pcomms(analogs, digitals):
    msg = pcomms_t()
    msg.num_analogs = len(analogs)
    for key, value in analogs.items():
        analog = analog_t()
        analog.name = key
        analog.value = value
        msg.analogs.append(analog)
    msg.num_digitals = len(digitals)
    for key, value in digitals.items():
        digital = digital_t()
        digital.name = key
        digital.value = value
        msg.digitals.append(digital)
    return msg


class WaypointTester(object):
    def __init__(self):
        self.lc = lcm.LCM()
        self.nav_sub = self.lc.subscribe("OPENINS_NAV_SOLUTION", self.handle_nav)
        # TODO: These are redundant...
        self.has_nav = False
        self.initialized = False

    def run(self):
        while not self.has_nav:
            self.lc.handle()
        # QUESTION: Do we need to keep the node alive for some period after the publication in order to be sure it completes?

    def initialize_projection(self, lon, lat):
        """
        Initialize the transverse mercator projection that is used within ROS.
        """
        proj_string = "+proj=tmerc +lat_0={} +lon_0={}".format(lat, lon)
        self.proj = pyproj.Proj(proj_string)
        self.initialized = True

    def local_xy_from_lonlat(self, lon, lat):
        easting, northing = self.proj(lon, lat)
        # NED coordinate frame
        return northing, easting

    def lonlat_from_local_xy(self, xx, yy):
        lon, lat = self.proj(yy, xx, inverse=True)
        return lon, lat

    def handle_nav(self, _, data):
        """
        Translate OPENINS_NAV_SOLN to ROS messages.

        The nav data includes more fields than any single standard ROS message.
        For now, publishing messages that already have corresponding rviz
        plugins, though we may want to add more.
        * tf
        * odometry
        """
        nav_msg = nav_solution_t.decode(data)
        if not self.initialized:
            init_lon, init_lat = nav_msg.initial_lonlat_fix
            self.initialize_projection(init_lon, init_lat)

        lon, lat, depth = nav_msg.absolute_position

        self.send_waypoints(lon, lat, nav_msg.unix_time)
        self.send_openmngr_cmd()

        self.has_nav = True

    def send_waypoints(self, lon, lat, tt):
        xx, yy = self.local_xy_from_lonlat(lon, lat)

        wypts_msg = water_object_list_t()
        wypts_msg.time_unix_sec = tt

        aa = analog_t()
        # aa.name = "UUID:{{{}}}".format(uuid.uuid1())  # This showed up in workspace, but we want something repeatable
        # aa.name = "UUID:{autonomous-mission}"   # This showed up in the WYPTS_STAT, but not in Workspace
        # aa.name = "UUID:{00000000-0000-0000-0ros-000mission}"   # Ditto ....
        aa.name = "UUID:{00000000-0000-0000-0000-00000000abcd}"  # Created new mission in Workspace too. SO, must be valid UUID.

        aa.value = 0.0
        wypts_msg.num_analogs = 1
        wypts_msg.analogs.append(aa)

        # QUESTION: What happens if a subsequent Mission is sent with fewer waypoints, but the same UUID?
        #   I'm guessing that the old waypoints will still persist at the end fo the mission?
        rr = 5
        wypt_idx = 0
        for idx, (dx, dy) in enumerate([(rr, rr), (rr, -rr), (-rr, -rr), (-rr, rr)]):
            # TODO: Create "make waypoint" function call to make this shorter =)
            for depth in [2, 4, 6]:
                obj_msg = water_object_t()

                # NB: in GSS's messages, the waypoint IDs are {########-####-####-####-############}
                # NB: Can't be all zeros, and must be valid UUID (hex)
                obj_msg.id_object = "{{00000000-abcd-0000-{:04d}-000000000000}}".format(
                    wypt_idx
                )
                # obj_msg.id_object = "{{{}}}".format(uuid.uuid1())

                obj_msg.obj_type = (
                    "WPT"  # Alternatives are SHIP, ROV, AUV, WPT, MOB, MARKER
                )
                obj_msg.name = "wpt_{:03d}".format(
                    wypt_idx
                )  # Sorted by name in Workspace; only used for display
                # obj_msg.note  # was blank in example; additional annotation

                obj_msg.position_valid = True
                obj_msg.position_coordinate_system = "geographic"  # Geographic is WGS84
                lon, lat = self.lonlat_from_local_xy(xx + dx, yy + dy)
                obj_msg.position_x = lon
                obj_msg.position_y = lat
                obj_msg.position_z = depth  # Depth
                # Controls whether Z is depth or altidude, independently of U_AUTOALT_EN, so long as U_ENALBE_WYPT_FBW_Z=False
                obj_msg.position_z_is_altitude = False

                # The comments make this seem like it should be enabled for any of the attitude
                # vars to be considered valid, but even with it False, the heading commands are obeyed.
                # * I double-checked the waypoints coming from Workspace, and even with a fixed
                #   heading they all have attitude_valid = False.
                # * I also tried settign this to True to see if Workspace would properly load
                #   heading values from my waypoints, but that didn't work either.
                obj_msg.attitude_valid = False

                obj_msg.attitude_roll_deg = 0.0
                obj_msg.attitude_pitch_deg = 0.0
                obj_msg.attitude_heading_deg = 90 * idx
                # True north or magnetic north
                obj_msg.attitude_heading_true_north = False

                # Given that this message is called "water_object_t", I assume
                # it's used for more than just waypoints, and these particular
                # fields are probably used for watch circles.
                obj_msg.safety_zone_enabled = False
                obj_msg.saftey_zone_radius_m = 0.0

                # This seems to be in m/s, NOT percentage?
                # It provides a CAP on the SPEED analog. (though the comments indicate that it's suppoed to be total thrust)
                obj_msg.waypoint_speed_pct = 1.0
                obj_msg.waypoint_tolerance_m = 0.15

                # obj_msg.waypoint_task = ""

                a1 = analog_t()
                a1.name = "HEADING_MODE"
                a1.value = 0.0
                obj_msg.analogs.append(a1)

                a2 = analog_t()
                a2.name = "Z_MATTERS"
                a2.value = 1.0
                obj_msg.analogs.append(a2)

                a3 = analog_t()
                a3.name = "SPEED"
                a3.value = 0.4
                obj_msg.analogs.append(a3)

                a4 = analog_t()
                a4.name = "EFFORT"
                a4.value = 70.0
                obj_msg.analogs.append(a4)

                a5 = analog_t()
                a5.name = "USE_SPEED"
                a5.value = 1.0
                obj_msg.analogs.append(a5)

                # Ignoring RED and BLUE
                a6 = analog_t()
                a6.name = "GREEN"
                a6.value = 255.0
                obj_msg.analogs.append(a6)

                obj_msg.num_analogs = len(obj_msg.analogs)

                wypts_msg.objects.append(obj_msg)
                wypt_idx += 1

        wypts_msg.num_objects = len(wypts_msg.objects)
        self.lc.publish("WYPTS_CMD", wypts_msg.encode())

    def send_openmngr_cmd(self):
        # First, need to be sure that the vehicle is in the correct state to follow the specified waypoints
        digitals = {
            # Disable hold so we progress through all waypoints.
            # Without this, we get an odd set of behaviors:
            # - If autos already enabled and hold enabled, vehicle will move in response to waypoint command
            # - If autos needed to be enabled and hold is enabled, vehicle will wait for a "next waypoint" command
            # - If hold was already disabled, vehicle would immediately transit to first waypoint
            "U_HOLD_CURRENT_WAYPOINT": False,
            "U_ENABLE_REACH_XYZ_TOLERANCE_MODE": True,  # Hoping this will force it to achieve depth before continuing
            # I hoped that this would cause it to constantly publish active mission,
            # but that didn't seem to work. I've emailed Greensea about this goal.
            # 'U_PUBLISH_MISSION': True,
            # Vertical control has two switches: disabled/enabled and altitude/depth
            "U_AUTOVERT_EN": True,  # enable vertical control
            "U_AUTOALT_EN": False,  # Whether to use altitude or depth reference. Use False for depth.
            "U_ENABLE_WYPT_FBW_Z": False,  # false will enable trajectory depth
            "U_AUTOHDG_EN": True,  # Enable heading control
            "U_ENABLE_WYPT_HEADING_TO_TGT": True,  # True enables the heading ref to come from path
            "C_ENABLE_DYNAMIC_POSITIONING": True,  # True turns XY control on
            "U_WYPT_EFFORT_OVERIDE": False,  # false disables the tortoise/hare override. Yes, the variable only has one R in overide
        }
        analogs = {
            # Reset to start following from start of mission.
            # Otherwise, with same mission ID, it'll start in the middle of the new mission.
            # Maybe we want waypoints to have unique IDs, so they're replaced rather than removed?
            # (Then deleting from Workspace is a pain...)
            "U_DRIVETO_WYPT": 0,
            # 'U_PROFILE_REQUEST': 6.0,  # Also sent when waypoint mode is turned ON; unsure if it's necessary
        }

        manager_msg = make_pcomms(analogs, digitals)

        self.lc.publish("OPENMNGR_CMD", manager_msg.encode())

        # next_msg = make_pcomms({}, {'U_INCREMENT_CURRENT_WAYPOINT': True})
        # self.lc.publish("OPENMNGR_CMD", next_msg.encode())


if __name__ == "__main__":
    wp_test = WaypointTester()
    wp_test.run()
